package com.example.mylittlegreenthumbapp

interface ILoginView:MvpView {
    fun onSuccess(loginBase:LoginResponse)

    fun onError(error: Error)
}