package com.example.mylittlegreenthumbapp

import android.content.Context
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignupPresenter(var iSignupView: ISignUpView,var context: Context):ISignUpPresenter {

    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()

    private lateinit var countryList : Array<CountryData>
    private lateinit var stateList : Array<StateData>
    private lateinit var cityList : Array<CityData>
    private lateinit var cntryId :String
    private lateinit var stateId:String
    private lateinit var cityId:String

    override fun registrationApi(firstName: String, lastName: String, phone: String, email: String, password: String, cntryId: Int, stateId: Int, cityId: String, userType: Int) {

        if (iSignupView.isNetworkConnected()) {

            RetrofitObject.instance.regList(firstName, lastName, phone, email, password, cntryId, stateId, cityId, userType)
                .enqueue(object : Callback<JsonObject> {
                    override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                        when {
                            response.code() == 400 -> {
                                val loginBase = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                                iSignupView.onSignupError(loginBase)

                            }
                            response.code() == 200 -> {
                                val loginBase = gson.fromJson(response.body().toString(), SignupResponse::class.java)
                                iSignupView.onSignupSuccess(loginBase)


                            }
                            else -> {

                            }

                        }
                    }

                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                    }

                })

        }
        else{


            iSignupView.showMessage(context.resources.getString(R.string.no_internet))
        }


    }

    override fun countryApi() {
        RetrofitObject.instance.countryList()
            .enqueue(object : Callback<JsonObject> {
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                    when{
                        response.code() == 200 -> {
                            iSignupView.onCountrySuccess(response)

                        }
                        response.code() == 400 -> {
                            val res = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                            iSignupView.onCountryError(res)
                        }

                    }

                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                }

            })
    }

    override fun itemCountrySpinner(cntryId: Int) {
        RetrofitObject.instance.stateList(cntryId)
            .enqueue(object : Callback<JsonObject> {
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                    when{

                        response.code() == 200 -> {
                            iSignupView.countrySpinnerSuccess(response)

                        }

                        response.code() == 400 -> {
                            val res = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                        }


                    }
                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    iSignupView.showMessage("Some Error")
                }

            })
    }

    override fun itemStateSpinner(stateId: Int) {
        RetrofitObject.instance.cityList(stateId)
            .enqueue(object : Callback<JsonObject> {
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                    when{

                        response.code() == 200 -> {
                            iSignupView.stateSpinnerSuccess(response)


                        }

                        response.code() == 400 -> {
                            val res = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                        }


                    }

                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                }

            })
    }

}