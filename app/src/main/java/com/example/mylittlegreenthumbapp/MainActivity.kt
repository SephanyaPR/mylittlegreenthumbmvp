package com.example.mylittlegreenthumbapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(),ILoginView {
    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()
    private val loginPresenter = LoginPresenter(this,this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        progressBar.visibility = View.GONE

        buttonLogin.setOnClickListener {


            var email = editTextEmail.text.toString().trim()
            var password = editTextPassword.text.toString().trim()
            var userType: Int = 1

            loginPresenter.callLoginApi(email, password, userType)

        }
        signupText.setOnClickListener {
            if (isNetworkConnected()) {
                startActivity(Intent(this, SignUpActivity::class.java))
            }
            else
            {
                showMessage(applicationContext.resources.getString(R.string.no_internet))
            }


        }

    }

    override fun onBackPressed() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
    override fun onSuccess(loginBase: LoginResponse) {
        showMessage(loginBase.message!!)
        if (loginBase.status=="success"){
            SharedPrefManager.getInstance(applicationContext).saveUser(loginBase.data!!)
            val intent = Intent(applicationContext,HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
           startActivity(intent)
        }
        else
        {
            showMessage("Something is Wrong")
        }

    }

    override fun onError(error: Error) {
        showMessage(error.message!!)
    }

}