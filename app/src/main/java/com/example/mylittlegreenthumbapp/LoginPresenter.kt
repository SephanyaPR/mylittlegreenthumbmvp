package com.example.mylittlegreenthumbapp

import android.content.Context
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPresenter (var iLoginView: ILoginView,var context: Context):ILoginPresenter{
    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()
    override fun callLoginApi(emailId: String, password: String, providerType: Int) {

        iLoginView.showLoading()

        if (iLoginView.isNetworkConnected()) {


            RetrofitObject.instance.login(emailId, password, 1)
                .enqueue(object : Callback<JsonObject> {
                    override fun onResponse(
                        call: Call<JsonObject>,
                        response: Response<JsonObject>
                    ) {
                        iLoginView.hideLoading()
                        when {
                            response.code() == 400 -> {
                                val loginBase = gson.fromJson(
                                    response.errorBody()?.charStream(),
                                    Error::class.java
                                )
                                iLoginView.onError(loginBase)
                            }
                            response.code() == 200 -> {
                                val loginBase = gson.fromJson(
                                    response.body().toString(),
                                    LoginResponse::class.java
                                )
                                iLoginView.onSuccess(loginBase)


                            }
                            else -> {
                                iLoginView.showMessage("Something went wrong")
                            }
                        }
                    }

                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                    }

                })


        }

        else {
            iLoginView.hideLoading()
            iLoginView.showMessage("Make sure that you have active internet connection")

        }

    }

}