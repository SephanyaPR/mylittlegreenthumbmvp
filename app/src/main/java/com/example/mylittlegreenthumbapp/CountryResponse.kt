package com.example.mylittlegreenthumbapp

data class CountryResponse (
    val data: List<CountryData>,
    val message: String,
    val status: String
)