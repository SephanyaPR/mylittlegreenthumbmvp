package com.example.mylittlegreenthumbapp

data class StateData (
    val state_id: String,
    val state_name: String
)