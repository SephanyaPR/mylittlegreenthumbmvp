package com.example.mylittlegreenthumbapp

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface Api {

    @FormUrlEncoded
    @POST("login")
    fun login(@Field("email_id") email_id: String, @Field("password") password: String,
              @Field("user_type") user_type: Int): Call<JsonObject>

    @POST("country")
    fun countryList(): Call<JsonObject>

    @FormUrlEncoded
    @POST("state")
    fun stateList(
        @Field("country_id") countryIDAPI:Int
    ) : Call<JsonObject>

    @FormUrlEncoded
    @POST("city")
    fun cityList(
        @Field("state_id") stateIDAPI:Int
    ) : Call<JsonObject>

    @FormUrlEncoded
    @POST("registration")
    fun regList(
        @Field("first_name") first_name:String,
        @Field("last_name") last_name:String,
        @Field("user_phone1") user_phone1:String,
        @Field("email_id") email_id:String,
        @Field("password") password:String,

        @Field("country") country:Int,
        @Field("state") state:Int,
        @Field("city") city:String,
        @Field("user_type") user_type: Int
    ) : Call<JsonObject>


}
