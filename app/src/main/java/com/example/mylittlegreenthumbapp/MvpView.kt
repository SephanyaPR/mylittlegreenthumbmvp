package com.example.mylittlegreenthumbapp

interface MvpView {
    fun showLoading()
    fun hideLoading()
    fun isNetworkConnected() : Boolean
    fun showMessage(message:String)
}