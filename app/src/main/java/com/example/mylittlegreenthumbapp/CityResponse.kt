package com.example.mylittlegreenthumbapp

data class CityResponse (
    val data: List<CityData>,
    val message: String,
    val status: String
)