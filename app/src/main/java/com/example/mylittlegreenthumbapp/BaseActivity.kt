package com.example.mylittlegreenthumbapp

import android.content.Context
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*

open class BaseActivity:AppCompatActivity(),MvpView{
    override fun isNetworkConnected() : Boolean {
        val connectivityManager=this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo=connectivityManager.activeNetworkInfo
        return  networkInfo?.isConnectedOrConnecting == true
    }

    override fun showLoading() {
        progressBar?.visibility = View.VISIBLE
    }
    override fun hideLoading() {
        progressBar?.visibility = View.GONE
    }

    override fun showMessage(message: String) {
        Toast.makeText(this,message, Toast.LENGTH_LONG).show()
    }
}