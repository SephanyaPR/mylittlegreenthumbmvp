package com.example.mylittlegreenthumbapp

data class CityData (
    val city_id: String,
    val city_name: String
)