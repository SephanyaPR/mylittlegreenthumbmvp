package com.example.mylittlegreenthumbapp

interface ISignUpPresenter {
    fun registrationApi(firstName:String,lastName:String,phone:String,email:String,password:String,cntryId: Int,stateId:Int,cityId:String
                        ,userType:Int)

    fun countryApi()

    fun itemCountrySpinner(cntryId:Int)
    fun itemStateSpinner(stateId: Int)
}