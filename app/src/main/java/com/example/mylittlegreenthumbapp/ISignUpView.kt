package com.example.mylittlegreenthumbapp

import com.google.gson.JsonObject
import retrofit2.Response

interface ISignUpView:MvpView {
    fun onSignupSuccess(loginBase:SignupResponse)
    fun onSignupError(error: Error)

    fun onCountrySuccess(response: Response<JsonObject>)
    fun onCountryError(loginBase: Error)

    fun countrySpinnerSuccess(response: Response<JsonObject>)
    fun stateSpinnerSuccess(response: Response<JsonObject>)

}