package com.example.mylittlegreenthumbapp

data class StateResponse (

    val data: List<StateData>,
    val message: String,
    val status: String
)