package com.example.mylittlegreenthumbapp

data class SignupResponse (val status: String, val message:String, val data: User)