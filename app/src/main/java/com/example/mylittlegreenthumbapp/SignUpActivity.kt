package com.example.mylittlegreenthumbapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_sign_up.*
import retrofit2.Response

class SignUpActivity : BaseActivity(),ISignUpView,AdapterView.OnItemSelectedListener {

    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()
    private lateinit var countryList : Array<CountryData>
    private lateinit var stateList : Array<StateData>
    private lateinit var cityList : Array<CityData>
    private  var cntryId :Int = 0
    private  var stateId:Int = 0
    private lateinit var cityId:String

    private val signUpPresenter = SignupPresenter(this,this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)


        toolbar.title = ""
        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        citySpinner.isEnabled = false

        countrySpinner.onItemSelectedListener = this
        stateSpinner.onItemSelectedListener = this
        citySpinner.onItemSelectedListener = this





        registerButtonMainActivity.setOnClickListener {

            var firstName = firstnameMainActivity.text.toString()
            var lastName = lastnameMainActivity.text.toString()
            var phone = phoneMainActivity.text.toString()
            var email = emailMainActivity.text.toString()
            var password = passwordMainActivity.text.toString()
            var confirmPass = cnfpasswordMainActivity.text.toString()
            var userType =1


            if(isNetworkConnected()) {

                signUpPresenter.registrationApi(firstName, lastName, phone, email, password, cntryId, stateId, cityId, userType)
            }
            else{
                showMessage("Connect to Internet")
            }
        }

        signUpPresenter.countryApi()

    }


    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        when(parent?.id) {
            R.id.countrySpinner -> {


                cntryId = countryList.get(position).country_id.toInt()


                if (cntryId == 1) {
                    cntryId = 1
                    stateSpinner.isEnabled = false
                    signUpPresenter.itemCountrySpinner(cntryId)
                    countrySpinner.setSelection(0)
                }
                else if(cntryId>1) {
                    stateSpinner.isEnabled = true
                    cntryId -= 1
                    signUpPresenter.itemCountrySpinner(cntryId)
                }



            }

            R.id.stateSpinner -> {
                stateId = stateList.get(position).state_id.toInt()



                if (position>=1){
                    citySpinner.isEnabled=true
                }
                else if (position<1){
                    citySpinner.isEnabled=false
                }


                if (stateId == 1) {
                    stateId = 1
                    signUpPresenter.itemStateSpinner(stateId)
                    stateSpinner.setSelection(0)
                } else if(stateId>1) {
                    stateId -= 1

                    signUpPresenter.itemStateSpinner(stateId)
                }

            }

            R.id.citySpinner -> {
                cityId = cityList.get(position).city_id

            }

        }

    }



    override fun onNothingSelected(parent: AdapterView<*>?) {

    }





    override fun onSignupSuccess(loginBase: SignupResponse) {
        Toast.makeText(applicationContext, loginBase.message, Toast.LENGTH_SHORT).show()
        startActivity(Intent(applicationContext,MainActivity::class.java))
    }

    override fun onSignupError(error: Error) {
        Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
    }

    override fun onCountrySuccess(loginBase: Response<JsonObject>) {

        val res = gson.fromJson(loginBase.body().toString(), CountryResponse::class.java)
        countryList = res.data.toTypedArray()


        var country = arrayOfNulls<String>(countryList.size+1)


        country[0] = "Select Country"

        for (i in countryList.indices) {
            country[i+1] = countryList[i].country_name
        }


        val adapter =  ArrayAdapter(this, android.R.layout.simple_spinner_item,country)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        countrySpinner.adapter = adapter




    }

    override fun onCountryError(loginBase: Error) {
        Toast.makeText(applicationContext, "Some error", Toast.LENGTH_SHORT).show()
    }

    override fun countrySpinnerSuccess(response: Response<JsonObject>) {
        val res = gson.fromJson(response.body().toString(), StateResponse::class.java)
        stateList = res.data.toTypedArray()
        var state = arrayOfNulls<String>(stateList.size+1)


        state[0]="Select State"
        for (i in stateList.indices) {


            state[i+1] = stateList[i].state_name
        }
        val adapter =  ArrayAdapter(this, android.R.layout.simple_spinner_item,state)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        stateSpinner.adapter = adapter
    }

    override fun stateSpinnerSuccess(response: Response<JsonObject>) {
        val res = gson.fromJson(response.body().toString(), CityResponse::class.java)
        cityList = res.data.toTypedArray()
        var city = arrayOfNulls<String>(cityList.size+1)

        city[0]="Select city"

        for (i in cityList.indices) {


            city[i+1] = cityList[i].city_name

        }

        val adapter =  ArrayAdapter(this, android.R.layout.simple_spinner_item,city)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        citySpinner.adapter = adapter
    }




}