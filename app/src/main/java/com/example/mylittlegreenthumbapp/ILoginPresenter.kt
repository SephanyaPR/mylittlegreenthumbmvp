package com.example.mylittlegreenthumbapp

interface ILoginPresenter {
    fun callLoginApi(emailId:String,password:String,providerType:Int)
}